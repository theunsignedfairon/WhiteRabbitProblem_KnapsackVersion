import java.util.ArrayList;


/**
 * Class which using for calculating the best way
 * for buying times
 */
public class WhiteRabbitTime {
    // Declaration of fields
    private int[][] matrix;                                         // Matrix for Dynamic Programming Approach
    private ArrayList<Item> items = new ArrayList<Item>();          // Array of items


    /**
     * Constructor for initialization
     * @param arrayOfValues array of values of items
     * @param arrayOfCosts array of costs of items
     */
    WhiteRabbitTime(ArrayList<Integer> arrayOfValues, ArrayList<Integer> arrayOfCosts) {
        // Filling the array with items
        for (int i = 0; i < arrayOfValues.size(); i++) {
            items.add(new Item(arrayOfValues.get(i), arrayOfCosts.get(i)));
        }
    }


    /**
     * Calculate the best way using Dynamic Programming Approach
     * @param budget Amount of money, which can be spend
     * @return return max amount of "time"
     */
    public int calculateDynamicProgramming(int budget) {
        matrix = new int[budget + 1][items.size() + 1]  ;                      // Initialization matrix with size (budget + 1) * (size of items + 1)
        for (int i = 1; i <= items.size(); i++) {                              // Outloop (from 2-nd element to last)
            for (int c = 1; c <= budget; c++) {                                // Inloop (from min cost to budget)
                if (items.get(i - 1).getCost() <= c) {                         // If out budget allows us to add one more element
                    matrix[c][i] = Math.max(matrix[c][i - 1], matrix[c - items.get(i - 1).getCost()][i - 1] + items.get(i - 1).getValue());
                } else {                                                       // Otherwise copy from previous cell
                    matrix[c][i] = matrix[c][i - 1];
                }
            }
        }

        return matrix[budget][items.size()];
    }


    /**
     * Calculate the best way using Brute Force Approach
     * @param budget Amount of money, which can be spend
     * @return return the best way
     */
    public int calculateBruteForce(int budget) {
        int result = 0;                                             // The max amount of time
        for (int i = 0; i < Math.pow(2, items.size()); i++) {       // We use binary sequence from 0 to (2^N) - 1
            String binaryString = Integer.toBinaryString(i);        // Convert Integer to binary string
            while (binaryString.length() != items.size()) {         // Add in the beginning of binary string appointin zero
                binaryString = "0" + binaryString;
            }
            int currentCost = 0, currentTime = 0;                   // Current time and cost
            for (int j = 0; j < binaryString.length(); j++) {
                if (binaryString.charAt(j) == '1') {                // If current char equals '1' then we add current item in bag
                    currentCost += items.get(j).getCost();
                    currentTime += items.get(j).getValue();
                }
            }
            if (currentTime > result && currentCost <= budget)     // Choose the best result
                result = currentTime;
        }
        return result;
    }


    /**
     * Calculate the best way using Greedy Approach
     * @param budget Amount of money, which can be spend
     * @return return the bset way
     */
    public int calculateGreedyApproach(int budget) {
        ArrayList<Item> tempArray = new ArrayList<Item>(items);                                                 // Temp array for sorting
        int maxCost = 0, maxTime = 0, index = 0;                                                                // max Time/Cost

        for (int i = 0; i < tempArray.size() - 1; i++) {                                                        // Loop for sorting array in
            double maxUnitWeight = (double)tempArray.get(i).getValue()/tempArray.get(i).getCost();              // descending order of unit weight
            int indexOfMaxUnitWeight = i;                                                                       // Selection sort
            for (int j = i + 1; j < tempArray.size(); j++) {                                                    // Loop for search max unit weight
                double currentUnitWeight = (double)tempArray.get(i).getValue()/tempArray.get(i).getCost();
                if (currentUnitWeight > maxUnitWeight) {
                    maxUnitWeight = currentUnitWeight;
                    indexOfMaxUnitWeight = j;
                }
            }

            // Swapping
            Item buf = tempArray.get(i);
            tempArray.set(i, tempArray.get(indexOfMaxUnitWeight));
            tempArray.set(indexOfMaxUnitWeight, buf);
        }

        // Greed Approach
        for (index = 0; index < tempArray.size() && maxCost <= budget; index++) {
            maxTime += tempArray.get(index).getValue();
            maxCost += tempArray.get(index).getCost();
        }

        maxTime -= tempArray.get(index - 1).getValue();             //Remove last element

        return maxTime;
    }
}
