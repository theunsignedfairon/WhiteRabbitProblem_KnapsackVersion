import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        // Input/Output stream for data
        Scanner sc = new Scanner(new FileReader("input.txt"));          // Initialized input stream
        PrintWriter wr = new PrintWriter("output.txt");                 // Initialized output stream

        // Declaration
        String[] elementsStr;                                                   // String of input data
        int budget;                                                             // Amount of money, which can spend
        ArrayList<Integer> arrayOfValues = new ArrayList<>();                   // Array of values
        ArrayList<Integer> arrayOfCosts = new ArrayList<>();                    // Array of costs
        WhiteRabbitTime myWhiteRabbitTime;                                      // Object of WhiteRabbitTime Class

        // Scan input data
        elementsStr = sc.nextLine().split(" ");
        budget = (int)sc.nextDouble();

        for (int i = 0; i < elementsStr.length; i++) {
            if (i % 2 == 0) {                                                   // If index is even, then we add value of item
                arrayOfValues.add(Integer.parseInt(elementsStr[i]));
            } else {
                arrayOfCosts.add(Integer.parseInt(elementsStr[i]));             // If index is odd, then we add cost of iyem
            }
        }

        // Calculation
        myWhiteRabbitTime = new WhiteRabbitTime(arrayOfValues, arrayOfCosts);

        //Print result in file
        wr.print(myWhiteRabbitTime.calculateDynamicProgramming(budget));


        // Close Input/Output stream
        wr.close();
        sc.close();
    }
}
